# Up and Running

This scaffold has been tested on node js v8; any version greater than v8 should work.

## For dev
`./node_modules/.bin/electron-webpack dev`

This starts the application with a development server. 

## For compilation
`./node_modules/.bin/electron-webpack`

## Building For Release
`yarn compile & ./node_modules/.bin/electron-builder`
