import Vue from "vue";

let eventBus = null;

const resolver = () => {
  if (eventBus === null) {
    eventBus = new Vue();
  }

  return {
    getBus: () => {
      return eventBus;
    }
  };
}

export default resolver();
