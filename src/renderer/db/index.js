const {app} = require("electron").remote;

let db = {
    getUserPath: () => {
        return app.getPath('userData');
    }
};

export {db};