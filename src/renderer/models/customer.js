const getNewMemberObject = () => {
    return {
        firstname: "",
        lastname: "",
        othernames: "",
        home_address: "",
        shop_address: "",
        nearest_busstop: "",
        phone_number: "",
        phone_number2: "",
        business_type: "",
        nickname: ""
    }
}

export {getNewMemberObject}