export default {
  items: [
    {
      name: 'Tradable',
      icon: 'icon-speedometer'
    },
    {
      name: 'Home',
      url: '/home',
      icon: 'icon-home'
    },
    {
      name: 'Customers',
      url: '/customers',
      icon: 'icon-user',
      children: [
        {
          name: 'List',
          url: '/customers/list',
          icon: 'icon-list'
        },
        {
          name: 'New Customer',
          url: '/customers/register',
          icon: 'icon-pencil'
        }
      ]
    },
    {
      name: 'Investors',
      url: '/investors',
      icon: 'icon-people',
      children: [
        {
          name: 'List',
          url: '/investors/list',
          icon: 'icon-list'
        },
        {
          name: 'Add Investor',
          url: '/investors/register',
          icon: 'icon-pencil'
        }
      ]
    },
    {
      name: 'Loans',
      url: '/loans',
      icon: 'icon-paper-plane'
    },
    {
      name: 'Investments',
      url: '/investments',
      icon: 'icon-chart'
    },
    {
      name: 'Account Statement',
      url: '/statment',
      icon: 'icon-calendar'
    },
    {
      name: 'Expenditure',
      url: '/expenditure',
      icon: 'fa fa-money'
    }
  ]
}
