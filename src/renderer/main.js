import Vue from "vue";
import VueRouter from "vue-router";
import VueLoading from "vue-loading-overlay";
import VeeValidate from 'vee-validate';
import BootstrapVue from "bootstrap-vue";
import Notifications from 'vue-notification';
import App from "./App.vue";
import router from "./router";

Vue.use(BootstrapVue);
Vue.use(VueRouter);
Vue.use(VueLoading);
Vue.use(VeeValidate, {fieldsBagName: 'formFields'});
Vue.use(Notifications);

/* eslint-disable no-new */
new Vue({
    el: "#app",
    router: router,
    template: "<App/>",
    components: {
        App
    }
});