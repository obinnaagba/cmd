import VueRouter, { RouterOptions } from "vue-router";

import DefaultContainer from "@/container/DefaultContainer.vue";

import CustomerRegister from "@/views/customers/CustomerRegister.vue";
import CustomerList from "@/views/customers/CustomerList.vue";
import CustomerDetail from "@/views/customers/CustomerDetail.vue";
import CustomerEdit from "@/views/customers/CustomerEdit.vue";
import CustomerLoans from "@/views/customers/CustomerLoans.vue";

import InvestorList from "@/views/investors/InvestorList.vue";
import InvestorDetail from "@/views/investors/InvestorDetail.vue";
import InvestorRegister from "@/views/investors/InvestorRegister.vue";
import InvestorEdit from "@/views/investors/InvestorEdit.vue";
import InvestorInvestments from "@/views/investors/InvestorInvestments.vue";

import LoanList from "@/views/loans/LoanList.vue";

import InvestmentList from "@/views/investments/InvestmentList.vue";

let routeObj = {
  mode: 'hash', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'open active',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: DefaultContainer,
      children: [
        {
          path: 'customers',
          redirect: 'customers/list',
          name: 'Customers',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'list',
              name: 'CustomerList',
              component: CustomerList
            },
            {
              path: 'register',
              name: 'CustomerRegister',
              component: CustomerRegister
            },
            {
              path: ':customerId/view',
              name: 'CustomerDetail',
              component: CustomerDetail,
              props: true
            },
            {
              path: ':customerId/edit',
              name: 'CustomerEdit',
              props: true,
              component: CustomerEdit
            },
            {
              path: ':customerId/loans',
              name: 'CustomerLoans',
              props: true,
              component: CustomerLoans
            }
          ]
        },
        {
          path: 'loans',
          redirect: 'loans/list',
          name: 'Loans',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'list',
              name: 'LoanList',
              component: LoanList
            },
            {
              path: ':loanId/view',
              name: 'LoanDetail',
              props: true
            }
          ]
        },
        {
          path: 'investors',
          redirect: 'investors/list',
          name: 'Investors',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'list',
              name: 'InvestorList',
              component: InvestorList
            },
            {
              path: 'register',
              name: 'InvestorRegister',
              props: true,
              component: InvestorRegister
            },
            {
              path: ':investorId/view',
              name: 'InvestorDetail',
              props: true,
              component: InvestorDetail
            },
            {
              path: ':investorId/edit',
              name: 'InvestorEdit',
              props: true,
              component: InvestorEdit
            },
            {
              path: ':investorId/investments',
              name: 'InvestorInvestments',
              props: true,
              component: InvestorInvestments
            }
          ]
        },
        {
          path: 'investments',
          redirect: 'investments/list',
          name: 'Investments',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'list',
              name: 'InvestmentsList',
              component: InvestmentList
            },
            {
              path: ':investmentId/view',
              name: 'InvestmentDetail',
              props: true
            }
          ]
        }
      ]
    }
  ]
};

let router = new VueRouter(routeObj);

export default router;
